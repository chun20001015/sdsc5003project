package com.MSDS.SDSC5003

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.MSDS.SDSC5003.utils.FireStoreUtils
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import kotlinx.android.synthetic.main.activity_login.*


//Show Login screen and its logic

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_UID = "uid"
    }

    private val fireStoreUtils = FireStoreUtils(null)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.DarkTheme)

        //check if the app is logged in already
        val uid = fireStoreUtils.getUID()
        if (uid != null) {
            goToMainActivity(uid)
        } else {
            supportActionBar?.hide()
            setContentView(R.layout.activity_login)
            btn_login.setOnClickListener(this)
        }
    }

    //perform actio when login button clicked
    override fun onClick(v: View?) {
        if(v?.id == btn_login.id) {
            val email = et_email.editText?.text.toString()
            val password = et_password.editText?.text.toString()

            //if email or password is empty, skip all the operation below
            if (email.isEmpty() || password.isEmpty()) return

            //set view
            progressBar.visibility = View.VISIBLE
            btn_login.visibility = View.INVISIBLE
            btn_login.isClickable = false

            fireStoreUtils.login(
                email, password,
                OnCompleteListener<AuthResult> { p0 ->
                    if (p0.isSuccessful) {
                        val uid = fireStoreUtils.getUID()
                        goToMainActivity(uid)
                    } else{
                        //reset
                        et_error.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE
                        btn_login.visibility = View.VISIBLE
                        btn_login.isClickable = true
                    }
                })
        }
    }

    //starting mainactivity screen
    private fun goToMainActivity(uid: String?) {
        val intent = Intent(baseContext, MainActivity::class.java)
        intent.putExtra(EXTRA_UID, uid)
        startActivity(intent)
        finish()
    }
}
