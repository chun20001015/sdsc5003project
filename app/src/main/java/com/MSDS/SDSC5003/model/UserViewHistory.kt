package com.MSDS.SDSC5003.model

import java.util.*

data class UserViewHistory (
    var user_id: String? = null,
    var movie_id: String? = null,
    var start_time: Calendar? = null,
    var end_time: Calendar? = null
)