package com.MSDS.SDSC5003.model

data class Movie (
    var id: String? = null,
    var synopsis: String? = null,
    var duration: String? = null,
    var genre: String? = null,
    var video_url: String? = null,
    var poster_url: String? = null,
    var title: String? = null,
    var release_year: String? = null,
    var actors: MutableList<String?> = mutableListOf()
)