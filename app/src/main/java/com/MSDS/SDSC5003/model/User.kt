package com.MSDS.SDSC5003.model

data class User (
    var uid: String? = null,
    var contact_email: String? = null,
    var username: String? = null
)