package com.MSDS.SDSC5003.model

data class Actor (
    var id: String? = null,
    var name: String? = null,
    var actor_url: String? = null
)