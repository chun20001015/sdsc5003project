package com.MSDS.SDSC5003

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.MSDS.SDSC5003.model.Movie
import com.MSDS.SDSC5003.ui.account.AccountFragment
import com.MSDS.SDSC5003.ui.home.HomeFragment
import com.MSDS.SDSC5003.ui.movie.MovieDetailFragment
import com.MSDS.SDSC5003.utils.FireStoreUtils
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

//This activity controlling the fragments

class MainActivity : AppCompatActivity(), HomeFragment.OnHomeFragmentInteractionListener,
    AccountFragment.OnAccountFragmentInteractionListener,
    MovieDetailFragment.onMovieDetailListener {

    lateinit var fireStoreUtils: FireStoreUtils


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.DarkTheme)

        //set up users information
        val uid = intent.getStringExtra(LoginActivity.EXTRA_UID)
        fireStoreUtils = FireStoreUtils(uid)

        setContentView(R.layout.activity_main)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_dashboard
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(false)
            setDisplayShowCustomEnabled(true)
            setDisplayShowTitleEnabled(false)

            val inflator = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val v = inflator.inflate(R.layout.custom_actionbar, null)
            val layout = ActionBar.LayoutParams(
                ActionBar.LayoutParams.FILL_PARENT,
                ActionBar.LayoutParams.FILL_PARENT
            )
            setCustomView(v, layout)
        }
    }

    //These function are the listener's callbacks from fragments
    override fun onMovieSelected(item: Movie) {
        openMovieDetailPage(item)
    }

    override fun onAccountFragmentInteraction(item: Movie) {
        openMovieDetailPage(item)
    }

    //Perform sign out action
    override fun onAccountFragmentSignOut() {
        fireStoreUtils.signout()
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    override fun onMovieBackPressed() {
        nav_view.visibility = View.VISIBLE
    }

    //Perform action when a movie is selected
    private fun openMovieDetailPage(item: Movie) {
        val nextFrag = MovieDetailFragment(item)
        supportFragmentManager.beginTransaction()
            .setCustomAnimations(
                android.R.anim.slide_in_left,
                android.R.anim.fade_out,
                android.R.anim.slide_in_left,
                android.R.anim.fade_out
            )
            .add(R.id.container, nextFrag, "findThisFragment")
            .addToBackStack(null)
            .commit()
        nav_view.visibility = View.GONE
    }

}
