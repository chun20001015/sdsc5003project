package com.MSDS.SDSC5003.ui.home


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.MSDS.SDSC5003.R
import com.MSDS.SDSC5003.model.Movie
import com.MSDS.SDSC5003.utils.setImageUrl
import kotlinx.android.synthetic.main.movie_item.view.*


class HomeRecyclerViewAdapter(
    var mValues: List<Movie>,
    private val mListener: HomeFragment.OnHomeFragmentInteractionListener?
) : RecyclerView.Adapter<HomeRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Movie
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onMovieSelected(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.movie_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.tvTitle.text = item.title
        holder.tvYear.text = item.release_year
        holder.tvGenre.text = item.genre
        item.poster_url?.let { holder.ivPoster.setImageUrl(it) }

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val tvTitle: TextView = mView.tv_title
        val tvYear: TextView = mView.tv_duration
        val tvGenre: TextView = mView.tv_genre
        val ivPoster: ImageView = mView.iv_poster

    }
}
