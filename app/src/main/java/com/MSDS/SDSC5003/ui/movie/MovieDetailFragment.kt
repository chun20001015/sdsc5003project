package com.MSDS.SDSC5003.ui.movie

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.MSDS.SDSC5003.MainActivity
import com.MSDS.SDSC5003.R
import com.MSDS.SDSC5003.model.Movie
import com.MSDS.SDSC5003.model.UserViewHistory
import com.MSDS.SDSC5003.utils.setImageUrl
import kotlinx.android.synthetic.main.fragment_movie_detail.*
import java.util.*

//This fragment shows the detail of specific movies

class MovieDetailFragment(val movie: Movie) : Fragment() {

    private lateinit var viewModel: MovieDetailViewModel
    private lateinit var listener: onMovieDetailListener

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProviders.of(this).get(MovieDetailViewModel::class.java)
        viewModel.fireStoreUtils = (activity as MainActivity).fireStoreUtils
        listener = activity as onMovieDetailListener
        return inflater.inflate(R.layout.fragment_movie_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Setting up data to the views
        iv_poster.setImageUrl(movie.poster_url ?: "")
        tv_title.text = movie.title
        tv_year.text = getString(R.string.movie_year, movie.release_year)
        tv_genre.text = movie.genre
        tv_description.text = movie.synopsis
        tv_duration.text = getString(R.string.movie_min, movie.duration)
        viewModel.actorsRelay.value?.let { mapActors ->
            if (movie.actors.isNotEmpty())
                movie.actors[0]?.let {
                    val actor = mapActors[it]
                    tv_actors.text = actor?.name ?: ""
                    tv_actors.setOnClickListener {
                        openURL(actor?.actor_url ?: "")
                    }
                }
        }
        //Record start time once play button is clicked
        btn_play.setOnClickListener {
            openURL(movie.video_url ?: "")
            viewModel.userViewHistory.movie_id = movie.id
            viewModel.userViewHistory.start_time = Calendar.getInstance()
        }


    }

    override fun onResume() {
        super.onResume()

        //Record end time and upload to the DB
        viewModel.userViewHistory.end_time = Calendar.getInstance()
        if (!viewModel.userViewHistory.movie_id.isNullOrEmpty() && viewModel.userViewHistory.start_time != null) {
            viewModel.fireStoreUtils.pushNewUserViewHistory(viewModel.userViewHistory)

            //clear data
            viewModel.userViewHistory = UserViewHistory()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        listener.onMovieBackPressed()
    }

    private fun openURL(url: String) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

    interface onMovieDetailListener {
        fun onMovieBackPressed()
    }


}
