package com.MSDS.SDSC5003.ui.account

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.MSDS.SDSC5003.MainActivity
import com.MSDS.SDSC5003.R
import com.MSDS.SDSC5003.model.Movie
import com.MSDS.SDSC5003.model.User
import com.MSDS.SDSC5003.model.UserViewHistory
import com.MSDS.SDSC5003.utils.CommonUtils
import com.MSDS.SDSC5003.utils.setCircleCrop
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.withLatestFrom
import kotlinx.android.synthetic.main.fragment_account.*
import kotlinx.android.synthetic.main.fragment_account.view.*

//This fragment shows the account info page

class AccountFragment : Fragment(), UpdateInfoDialogFragment.Listener {

    private lateinit var accountViewModel: AccountViewModel

    private var listener: OnAccountFragmentInteractionListener? = null

    private val disposables = CompositeDisposable()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        accountViewModel =
            ViewModelProviders.of(this).get(AccountViewModel::class.java)
        accountViewModel.fireStoreUtils = (activity as MainActivity).fireStoreUtils
        val root = inflater.inflate(R.layout.fragment_account, container, false)

        with(root.recyclerView) {
            layoutManager = LinearLayoutManager(context)
            adapter = HistoriesRecyclerAdapter(arrayListOf(), listener)
        }
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //setting the icon of the user
        iv_avatar.setCircleCrop(R.drawable.avatar)
        accountViewModel.userRelay.distinctUntilChanged().subscribe {
            tv_displayname.text = getString(R.string.welcome, it.username ?: "Unknown")
            tv_emailinfo.text = it.contact_email ?: "Unknown"
        }.addTo(disposables)

        //setting view histories data to the history list
        accountViewModel.userViewHistoriesRelay
            .withLatestFrom(accountViewModel.moviesRelay)
            .distinctUntilChanged().subscribe { (userViewHistories, movies) ->
                with(recyclerView.adapter as HistoriesRecyclerAdapter) {
                    mValues = formatHistoriesModel(userViewHistories, movies)
                    notifyDataSetChanged()
                }
        }.addTo(disposables)

        //Show user info update dialog when button clicked
        btn_update.setOnClickListener {
            val user = accountViewModel.userRelay.value
            UpdateInfoDialogFragment.newInstance(
                this,
                user?.username ?: "",
                user?.contact_email ?: ""
            ).show(fragmentManager, "dialog")
        }

        //sign out when button clicked
        btn_logout.setOnClickListener {
            listener?.onAccountFragmentSignOut()
        }
    }

    override fun onUpdateInfoClicked(user: User) {
        accountViewModel.updateUserInfo(user)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnAccountFragmentInteractionListener) {
            listener = context
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        listener = null
        disposables.clear()
    }

    interface OnAccountFragmentInteractionListener {
        fun onAccountFragmentInteraction(item: Movie)
        fun onAccountFragmentSignOut()
    }

    //Contruct a model that fits the View
    private fun formatHistoriesModel(
        mapHistories: MutableMap<String?, UserViewHistory>,
        mapMovies: HashMap<String?, Movie>
    ): List<HistoriesModel> {
        val historiesModel = arrayListOf<HistoriesModel>()
        mapHistories.forEach { (_, history) ->
            val startDate = history.start_time
            val endDate = history.end_time
            historiesModel.add(
                HistoriesModel(
                    uid = history.user_id,
                    startDate = if (startDate != null) CommonUtils.getSimpleDate(startDate) else "Unknown",
                    endDate = if (endDate != null) CommonUtils.getSimpleDate(endDate) else "Unknown",
                    movie = mapMovies.get(history.movie_id) ?: Movie()
                )
            )
        }
        return historiesModel.reversed()
    }
}