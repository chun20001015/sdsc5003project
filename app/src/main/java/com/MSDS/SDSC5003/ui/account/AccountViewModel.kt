package com.MSDS.SDSC5003.ui.account

import androidx.lifecycle.ViewModel
import com.MSDS.SDSC5003.model.Movie
import com.MSDS.SDSC5003.model.User
import com.MSDS.SDSC5003.model.UserViewHistory
import com.MSDS.SDSC5003.utils.FireStoreUtils
import com.jakewharton.rxrelay2.BehaviorRelay

class AccountViewModel : ViewModel() {

    lateinit var fireStoreUtils: FireStoreUtils

    val userRelay: BehaviorRelay<User>
        get() = fireStoreUtils.userRelay

    val userViewHistoriesRelay: BehaviorRelay<MutableMap<String?, UserViewHistory>>
        get() = fireStoreUtils.userViewHistoriesRelay

    val moviesRelay: BehaviorRelay<HashMap<String?, Movie>>
        get() = fireStoreUtils.moviesRelay

    fun updateUserInfo(user: User) {
        fireStoreUtils.updateUserInfo(user)
    }

}