package com.MSDS.SDSC5003.ui.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.MSDS.SDSC5003.R
import com.MSDS.SDSC5003.model.User
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_updateinfo_dialog.*


class UpdateInfoDialogFragment(
    var mListener: Listener?,
    val oldName: String?,
    val oldEmail: String?
) : BottomSheetDialogFragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_updateinfo_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        et_displayname.setText(oldName)
        et_email.setText(oldEmail)
        btn_apply.setOnClickListener {
            mListener?.onUpdateInfoClicked(
                User(
                    username = et_displayname.text.toString(),
                    contact_email = et_email.text.toString()
                )
            )
            dismiss()
        }
    }

    override fun onDetach() {
        mListener = null
        super.onDetach()
    }

    interface Listener {
        fun onUpdateInfoClicked(user: User)
    }

    companion object {
        fun newInstance(
            listener: Listener,
            displayname: String?,
            email: String?
        ): UpdateInfoDialogFragment =
            UpdateInfoDialogFragment(listener, displayname, email)
    }
}
