package com.MSDS.SDSC5003.ui.home

import androidx.lifecycle.ViewModel
import com.MSDS.SDSC5003.model.Movie
import com.MSDS.SDSC5003.utils.FireStoreUtils
import com.jakewharton.rxrelay2.BehaviorRelay

class HomeViewModel() : ViewModel() {

    lateinit var fireStoreUtils: FireStoreUtils

    val movies: BehaviorRelay<HashMap<String?, Movie>>
        get() = fireStoreUtils.moviesRelay




}