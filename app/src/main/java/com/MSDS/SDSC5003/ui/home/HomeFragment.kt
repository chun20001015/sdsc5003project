package com.MSDS.SDSC5003.ui.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.MSDS.SDSC5003.MainActivity
import com.MSDS.SDSC5003.R
import com.MSDS.SDSC5003.model.Movie
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*

//This fragment shows a movie list

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    private var listener: OnHomeFragmentInteractionListener? = null

    private val disposables = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        homeViewModel.fireStoreUtils = (activity as MainActivity).fireStoreUtils

        val root = inflater.inflate(R.layout.fragment_home, container, false)

        with(root.recyclerView) {
            val movies = homeViewModel.movies.value as HashMap
            layoutManager = LinearLayoutManager(context)
            adapter = HomeRecyclerViewAdapter(ArrayList(movies.values), listener)
        }
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Construct the movie list
        homeViewModel.movies.distinctUntilChanged().subscribe {
            if (it.count() > 0 ) progressBar.visibility = View.GONE
            with(recyclerView.adapter as HomeRecyclerViewAdapter){
                mValues = ArrayList(it.values)
                notifyDataSetChanged()
            }
        }.addTo(disposables)

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnHomeFragmentInteractionListener) {
            listener = context
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    interface OnHomeFragmentInteractionListener {
        fun onMovieSelected(item: Movie)
    }
}