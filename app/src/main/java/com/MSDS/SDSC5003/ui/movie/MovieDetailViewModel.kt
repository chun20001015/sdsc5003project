package com.MSDS.SDSC5003.ui.movie

import androidx.lifecycle.ViewModel
import com.MSDS.SDSC5003.model.Actor
import com.MSDS.SDSC5003.model.UserViewHistory
import com.MSDS.SDSC5003.utils.FireStoreUtils
import com.jakewharton.rxrelay2.BehaviorRelay

class MovieDetailViewModel : ViewModel() {

    lateinit var fireStoreUtils: FireStoreUtils

    val actorsRelay: BehaviorRelay<HashMap<String?, Actor>>
        get() = fireStoreUtils.actorsRelay

    var userViewHistory = UserViewHistory()

}
