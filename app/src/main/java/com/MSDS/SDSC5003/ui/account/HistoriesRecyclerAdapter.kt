package com.MSDS.SDSC5003.ui.account


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.MSDS.SDSC5003.R
import com.MSDS.SDSC5003.model.Movie
import kotlinx.android.synthetic.main.history_item.view.*
import kotlinx.android.synthetic.main.movie_item.view.tv_title


class HistoriesRecyclerAdapter(
    var mValues: List<HistoriesModel>,
    private val mListener: AccountFragment.OnAccountFragmentInteractionListener?
) : RecyclerView.Adapter<HistoriesRecyclerAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            //empty for now
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.history_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.tvTitle.text = item.movie.title
        holder.tvDate.text = item.startDate

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val tvTitle: TextView = mView.tv_title
        val tvDate: TextView = mView.tv_date
    }
}

data class HistoriesModel(
    var uid: String? = null,
    var startDate: String,
    var endDate: String,
    var movie: Movie = Movie()
)