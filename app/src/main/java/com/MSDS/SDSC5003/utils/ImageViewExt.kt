package com.MSDS.SDSC5003.utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

//This is a extension class for ImageView


fun ImageView.setImageUrl(url: String) {
    Glide.with(this)
        .load(url)
        .into(this)
}

fun ImageView.setCircleCrop(resource: Int) {
    Glide.with(this)
        .load(resource)
        .apply {
            apply(RequestOptions.circleCropTransform())
        }
        .into(this)
}