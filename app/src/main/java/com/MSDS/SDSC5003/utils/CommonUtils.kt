package com.MSDS.SDSC5003.utils

import java.text.SimpleDateFormat
import java.util.*


class CommonUtils {
    companion object {
        fun getCalendar(timestamp: Long) : Calendar{
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = timestamp
            return calendar
        }

        fun getSimpleDate(calendar: Calendar): String {
            val format = SimpleDateFormat("yyyy-MM-dd hh:mm:ss a")
            return format.format(calendar.time)

        }
    }
}