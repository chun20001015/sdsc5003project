package com.MSDS.SDSC5003.utils

import android.util.Log
import com.MSDS.SDSC5003.model.Actor
import com.MSDS.SDSC5003.model.Movie
import com.MSDS.SDSC5003.model.User
import com.MSDS.SDSC5003.model.UserViewHistory
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.jakewharton.rxrelay2.BehaviorRelay
import java.util.*
import kotlin.collections.HashMap


//All Firebase interaction perform here

class FireStoreUtils(uid: String?) {

    val userRelay = BehaviorRelay.createDefault(User(uid = uid))
    val actorsRelay = BehaviorRelay.createDefault(HashMap<String?, Actor>())
    val moviesRelay = BehaviorRelay.createDefault(HashMap<String?, Movie>())
    val genresRelay = BehaviorRelay.createDefault(HashMap<String?, String>())
    val actInMovieRelay = BehaviorRelay.createDefault(HashMap<String, MutableList<String?>>())
    val userViewHistoriesRelay =
        BehaviorRelay.createDefault(mutableMapOf<String?, UserViewHistory>())

    private val TAG = "FireStoreUtils"
    private val db: FirebaseFirestore = FirebaseFirestore.getInstance()
    private val mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private val userRef = db.collection("Users")
    private val actorsRef = db.collection("Actors")
    private val moviesRef = db.collection("Movies")
    private val actInMovieRef = db.collection("Act_in_movie")
    private val genresRef = db.collection("Genres")
    private val watchHistoriesRef =
        db.collection("watch_histories")

    init {
        if (uid != null) {
            //extract relevant user data
            userRef
                .document(uid)
                .addSnapshotListener { snapshot, e ->
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e)
                        return@addSnapshotListener
                    }


                    snapshot?.apply {
                        val user = User(
                            contact_email = get("contact_email") as String?,
                            username = get("username") as String?,
                            uid = id
                        )
                        userRelay.accept(user)
                        Log.d(TAG, "Users data: ${user}")
                    }
                }

            //extract all genres
            genresRef.addSnapshotListener { snapshot, e ->
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e)
                    return@addSnapshotListener
                }

                val genres = HashMap<String?, String>()
                snapshot?.documents?.forEach {
                    genres[it.id] = it.get("genre") as String
                }
                genresRelay.accept(genres)
                Log.d(TAG, "Genres data: ${genres}")
            }

            //extract all data in Act In Movie table
            actInMovieRef.addSnapshotListener { snapshot, e ->
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e)
                    return@addSnapshotListener
                }

                val actInMovie = HashMap<String, MutableList<String?>>()
                snapshot?.documents?.forEach {
                    val movieID = it.get("movie_id") as String
                    if (actInMovie[movieID].isNullOrEmpty()) actInMovie[movieID] = mutableListOf()
                    actInMovie[movieID]?.add(it.get("actor_id") as String)
                }
                actInMovieRelay.accept(actInMovie)
                Log.d(TAG, "actInMovie data: ${actInMovie}")
            }

            //extract actors info
            actorsRef.addSnapshotListener { snapshot, e ->
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e)
                    return@addSnapshotListener
                }

                val actors = HashMap<String?, Actor>()
                snapshot?.documents?.forEach {
                    actors[it.id] = Actor(
                        id = it.id,
                        name = it.get("name") as String?,
                        actor_url = it.get("actor_url") as String?
                    )
                }
                actorsRelay.accept(actors)
                Log.d(TAG, "Actors data: ${actors}")
            }

            //extract movies info
            moviesRef.addSnapshotListener { snapshot, e ->
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e)
                    return@addSnapshotListener
                }

                val genres = genresRelay.value ?: HashMap()
                val movies = HashMap<String?, Movie>()
                snapshot?.documents?.forEach {
                    movies[it.id] = Movie(
                        id = it.id,
                        synopsis = it.get("synopsis") as String?,
                        duration = it.get("duration") as String?,
                        genre = genres[it.get("genre") as String],
                        video_url = it.get("video_url") as String?,
                        poster_url = it.get("poster_url") as String?,
                        title = it.get("title") as String?,
                        release_year = it.get("release_year") as String?,
                        actors = actInMovieRelay.value?.get(it.id) ?: mutableListOf()
                    )
                }
                moviesRelay.accept(movies)
                Log.d(TAG, "Movies data: ${movies}")
            }

            //extract user watching histories
            watchHistoriesRef
                .whereEqualTo("user_id", userRelay.value?.uid!!)
                .addSnapshotListener { snapshot, e ->
                    if (e != null) {
                        Log.w(TAG, "Listen failed.", e)
                        return@addSnapshotListener
                    }

                    val userViewHistories = mutableMapOf<String?, UserViewHistory>()
                    snapshot?.documents?.forEachIndexed { index, it ->
                        val startDateTS = it.get("start_time") as Date
                        val endDateTS = it.get("end_time") as Date

                        userViewHistories[it.id] = UserViewHistory(
                            user_id = it.get("user_id") as String?,
                            movie_id = it.get("movie_id") as String?,
                            start_time = CommonUtils.getCalendar(startDateTS.time),
                            end_time = CommonUtils.getCalendar(endDateTS.time)
                        )
                    }

                    //sort watching histories
                    val sortedhsitories =
                        userViewHistories.toSortedMap(compareBy { userViewHistories[it]!!.start_time!!.timeInMillis })

                    userViewHistoriesRelay.accept(sortedhsitories)
                    Log.d(TAG, "userViewHistories data: ${sortedhsitories}")
                }
        }
    }

    //update user info in the DB
    fun updateUserInfo(user: User) {
        userRef
            .document(userRelay.value?.uid!!)
            .set(
                UserUpdateModel(
                    username = user.username ?: "",
                    contact_email = user.contact_email ?: ""
                ), SetOptions.merge()
            )
    }

    //add movie watching histories to the DB
    fun pushNewUserViewHistory(userViewHistory: UserViewHistory) {
        val mid = userViewHistory.movie_id
        val startDate = userViewHistory.start_time
        val endDate = userViewHistory.end_time
        if (!mid.isNullOrEmpty() && startDate != null && endDate != null) {
            watchHistoriesRef.add(
                PushHistoryModel(
                    movie_id = mid,
                    start_time = startDate.time,
                    end_time = endDate.time,
                    user_id = userRelay.value?.uid!!
                )
            )
            Log.d(TAG, "pushNewUserViewHistory $userViewHistory")
        }

    }

    fun getUID(): String? {
        return mAuth.currentUser?.uid
    }

    fun login(email: String, password: String, listener: OnCompleteListener<AuthResult>) {
        mAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(listener)
    }

    fun signout() {
        mAuth.signOut()
    }

    data class PushHistoryModel(
        val movie_id: String,
        val start_time: Date,
        val end_time: Date,
        val user_id: String
    )

    data class UserUpdateModel(
        val contact_email: String,
        val username: String
    )
}